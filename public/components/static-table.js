Vue.component('static-table', {
    props: ['headers', 'rows', 'headline'],
    template: `
<div>
  <h1>{{headline}}</h1>
  <table class="table">
   <thead>
     <tr>
       <th scope="col" v-for="header in headers">{{header}}</th>
     </tr>
   </thead>
   <tbody>
     <tr v-for="row in rows">
       <th v-for="entry in row">{{entry}}</th>
     </tr>
   </tbody>
  </table>
</div>`
});
