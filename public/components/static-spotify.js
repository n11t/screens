Vue.component('static-spotify', {
    props: ['link'],
    template: `
<div class="embed-responsive embed-responsive-16by9">
    <iframe id="spotify" :src="link" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
</div>
`,
    created: function () {
        let hlp = {
            searching: undefined,
        };
        hlp.seaching = setInterval(() => {
            $("button[title=Play]").click();
        }, 500);

    }
});
