Vue.component('static-html', {
    props: ['html'],
    template: `
<div v-html="html"></div>
`
});
