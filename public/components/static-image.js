Vue.component('static-image', {
    props: ['src'],
    template: `
<div class="row">
    <div class="col">
        <img :src="src" class="img-fluid" alt="Responsive image">
    </div>
</div>`
});
