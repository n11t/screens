Vue.component('static-youtube', {
    props: ['link'],
    template: `
<div class="embed-responsive embed-responsive-16by9">
    <iframe :src="link" allow="autoplay"></iframe>
</div>
`
});
