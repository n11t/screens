new Vue({
    el: '#app',
    data: function () {
        return {
            polling: undefined,
            screenId: undefined,
            componentName: undefined,
            componentProps: {}
        }
    },
    template: `
<div>
    <template>
        <component :is="componentName" v-bind="componentProps"></component>
    </template>
</div>`,
    methods: {
        pollData() {
            let vm = this;
            this.polling = setInterval(() => {
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        let arr = JSON.parse(this.responseText);
                        if (null !== arr.data && arr.data.id !== this.screenId) {
                            vm.componentName = arr.data.type;
                            vm.componentProps = arr.data.attributes;
                        }
                    }
                };
                xhttp.open("GET", "/screen.json", true);
                xhttp.send();
            }, 5000);
        }
    },
    created() {
        this.pollData();
    },
    beforeDestroy() {
        clearInterval(this.polling);
    }
});
