# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Added client to screen assignment.
- Client class

### Changed
- Route `/screen` was moved to `/`

## [0.1.0] - 2019-10-09
### Added
- Markdown screen.
- Image screen.
- Table screen.
- YouTube screen.

[Unreleased]: https://github.com/n11t/screens/compare/v0.1.0...HEAD
[0.1.0]: https://gitlab.com/n11t/screens/-/tags/v0.1.0
