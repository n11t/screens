# Screens

Screens ist eine Software um auf mehreren Bildschirmen gleichzeitig verschiedene Inhalte anzuzeigen.

## Installation

### Projekt klonen

Das Projekt als zip herunterladen oder via git eine lokale Kopie erstellen.

```
git clone https://gitlab.com/n11t/screens
```

### Abhängigkeiten installieren

Die Projektabhängigkeiten werden via composer installiert
```
composer install
```

### Konfiguration

Die Konfiguration wird über die `.env.local*`-Datei im Hauptordner vorgenommen.  
Dokumentation und Standartwerte hierzu befinden sich in der `.env`-Datei. Alle Werte können auch 
via Umgebungsvariale gesetzt werden.

Eine locale `.env.local.php` Datei kann man mit composer erzeugen.
```
composer dump-env env
```
`env` kann entweder `dev` oder `prod` sein. Wobei zu beachten ist, dass unter `prod` der php-Webserver nicht geladen wird.

### Datenbank erstellen

Die Datenbank wird, wenn nicht manuell erstellt, via doctrine erstellt.
```
bin/console doctrine:database:create
```

### Datenbank update

Das Datenbank-Schema wird mit doctrine erstellt.
```
bin/console doctrine:schema:update
```

## Nutzung

### Server

Der Webserver muss sein Dokumentroot auf `./public` legen und
Dateien, welche darunter vorhanden sind, freigeben.
Informationen dazu findet man in der [Symfony Dokumentation](https://symfony.com/doc/current/setup/web_server_configuration.html).

Zum Testen, Environemnt muss `dev` sein, kann man auch einen php-Server `bin/console server:start 80` starten.

### Client

Die Clients rufen `example.com/` auf und aktualisieren sich dann selbst.

### Screens anlegen

Screens werden via Console angelegt.
Hierzu findet man mehrere Commands unter `bin/console`.
Um zum Beispiel ein YouTube-Video anzulegen:  
`bin/console screen:youtube:create DLzxrzFCyOs`.
