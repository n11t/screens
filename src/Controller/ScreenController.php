<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Client;
use App\Exception\ClientNotFound\ClientNotFoundByIdException;
use App\Exception\ClientNotLoggedInException;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class ScreenController extends AbstractController
{

    /**
     * @param SessionInterface $session
     * @param ClientRepository $clientRepository
     * @param KernelInterface $kernel
     * @return Response
     * @throws \Exception
     */
    public function indexAction(SessionInterface $session, ClientRepository $clientRepository, KernelInterface $kernel)
    {
        $clientId = $session->get('client.id');
        if (!is_int($clientId)) {
            throw new ClientNotLoggedInException();
        }
        $client = $clientRepository->find($clientId);
        if (!$client instanceof Client) {
            throw new ClientNotFoundByIdException($clientId);
        }

        $rootDir = $kernel->getProjectDir();

        $content = file_get_contents($rootDir . '/public/index.html');

        return new Response($content);
    }
}
