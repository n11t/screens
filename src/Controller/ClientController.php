<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Client;
use App\Exception\ClientNotFound\ClientNotFoundByNameException;
use App\Exception\ClientNotFoundException;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ClientController extends AbstractController
{

    /**
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @param SessionInterface $session
     * @return Response
     * @throws ClientNotFoundException
     */
    public function loginAction(
        Request $request,
        ClientRepository $clientRepository,
        SessionInterface $session
    ): Response {
        $name = $request->get('name', '');

        $client = $clientRepository->findOneBy([
            'name' => $name,
        ]);
        if (!$client instanceof Client) {
            throw new ClientNotFoundByNameException($name);
        }

        $session->set('client.id', $client->getId());
        $session->set('client.name', $client->getName());

        $path = $this->generateUrl('index');

        return new RedirectResponse($path);
    }
}
