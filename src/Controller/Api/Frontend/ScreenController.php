<?php
declare(strict_types=1);

namespace App\Controller\Api\Frontend;

use App\Presenter\ScreenPresenter;
use App\Screen\Input\SessionScreenForClientInput;
use App\Screen\Output\ScreenForClientOutput;
use App\Screen\UseCase\ScreenForClientUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ScreenController extends AbstractController
{

    public function indexAction(
        SessionInterface $session,
        ScreenForClientUseCase $useCase,
        ScreenPresenter $screenPresenter
    ): Response {
        $input = new SessionScreenForClientInput($session);
        $output = new ScreenForClientOutput();

        $useCase->process($input, $output);

        $json = $screenPresenter->apiJson($output->screen);

        return new JsonResponse($json);
    }
}
