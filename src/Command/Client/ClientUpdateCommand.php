<?php
declare(strict_types=1);

namespace App\Command\Client;

use App\Client\Exception\ClientUpdate\ClientNotFoundException;
use App\Client\Exception\ClientUpdate\ClientValidationException;
use App\Client\Exception\ClientUpdate\DuplicateNameException;
use App\Client\Exception\ClientUpdateException;
use App\Client\Input\CliClientUpdateInput;
use App\Client\Output\CliClientUpdateOutput;
use App\Client\UseCase\ClientUpdateUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

class ClientUpdateCommand extends Command
{

    /**
     * @var ClientUpdateUseCase
     */
    private $clientUpdate;

    public function __construct(ClientUpdateUseCase $clientUpdate)
    {
        parent::__construct();

        $this->clientUpdate = $clientUpdate;
    }

    protected function configure()
    {
        $this
            ->setName('client:update')
            ->setDescription('Update given client')
            ->addArgument('id', InputArgument::REQUIRED, 'The client it to update')
            ->addOption('name', null, InputOption::VALUE_REQUIRED, 'The new client name', null)
        ;
    }

    protected function execute(InputInterface $cliInput, OutputInterface $cliOutput)
    {
        $input = new CliClientUpdateInput($cliInput);
        $output = new CliClientUpdateOutput($cliOutput);

        try {
            $this->clientUpdate->process($input, $output);
        } catch (ClientUpdateException $exception) {
            $this->handleClientUpdateException($exception, $cliOutput);
        }
    }

    private function handleClientUpdateException(ClientUpdateException $exception, OutputInterface $cliOutput): void
    {
        $messages = [];
        if ($exception instanceof DuplicateNameException) {
            $messages[] = sprintf('Name %s is already in use', $exception->getName());
        } elseif ($exception instanceof ClientValidationException) {
            $violations = $exception->getViolations();
            /** @var ConstraintViolationInterface $violation */
            foreach ($violations as $violation) {
                $messages[] = sprintf(
                    '%s: %s',
                    $violation->getPropertyPath(),
                    $violation->getMessage()
                );
            }
        } elseif ($exception instanceof ClientNotFoundException) {
            $messages[] = sprintf(
                'Client with id %d not found',
                $exception->getId()
            );
        } else {
            $messages[] = $exception->getMessage();
        }

        array_walk($messages, function (string &$message) {
            $message = sprintf('<error>%s</error>', $message);
        });
        $cliOutput->writeln($messages);
    }
}
