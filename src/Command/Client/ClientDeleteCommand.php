<?php
declare(strict_types=1);

namespace App\Command\Client;

use App\Client\Input\CliClientDeleteInput;
use App\Client\UseCase\ClientDeleteUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClientDeleteCommand extends Command
{

    /**
     * @var ClientDeleteUseCase
     */
    private $useCase;

    public function __construct(ClientDeleteUseCase $useCase)
    {
        parent::__construct();

        $this->useCase = $useCase;
    }

    protected function configure()
    {
        $this
            ->setName('client:delete')
            ->setDescription('Delete specified client.')
            ->addArgument('id', InputArgument::REQUIRED, 'The client id to remove.')
        ;
    }

    protected function execute(InputInterface $cliInput, OutputInterface $output)
    {
        $input = new CliClientDeleteInput($cliInput);

        $this->useCase->process($input);
    }
}
