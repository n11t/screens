<?php
declare(strict_types=1);

namespace App\Command\Client;

use App\Client\Exception\ClientInfo\ClientNotFoundException;
use App\Client\Exception\ClientInfoException;
use App\Client\Input\CliClientInfoInput;
use App\Client\Output\CliClientInfoOutput;
use App\Client\UseCase\ClientInfoUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClientInfoCommand extends Command
{

    /**
     * @var ClientInfoUseCase
     */
    private $useCase;

    public function __construct(ClientInfoUseCase $useCase)
    {
        parent::__construct();
        $this->useCase = $useCase;
    }

    protected function configure()
    {
        $this
            ->setName('client:detail')
            ->addArgument('client_id', InputArgument::REQUIRED, 'The client id to search for.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cliInput = new CliClientInfoInput($input);
        $cliOutput = new CliClientInfoOutput($output);

        try {
            $this->useCase->process($cliInput, $cliOutput);

            $cliOutput->render();
        } catch (ClientInfoException $exception) {
            $this->handleClientInfoException($output, $exception);
        }
    }

    private function handleClientInfoException(OutputInterface $output, ClientInfoException $exception)
    {
        if ($exception instanceof ClientNotFoundException) {
            $message = sprintf('<error>Client not found for id "%d".</error>', $exception->getId());
            $output->writeln($message);
        } else {
            $message = sprintf('<error>%s</error>', $exception->getMessage());
            $output->writeln($message);
        }
    }
}
