<?php
declare(strict_types=1);

namespace App\Command\Client;

use App\Repository\ClientRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClientListCommand extends Command
{

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        parent::__construct();

        $this->clientRepository = $clientRepository;
    }

    protected function configure()
    {
        $this
            ->setName('client:list')
            ->setDescription('List all clients')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $clients = $this->clientRepository->findAll();

        $table = new Table($output);
        $table->setHeaders(['#', 'Name', 'Created at']);
        foreach ($clients as $client) {
            $table->addRow([
                $client->getId(),
                $client->getName(),
                $client->getCreatedAt()->format('c')
            ]);
        }

        $table->render();
    }
}
