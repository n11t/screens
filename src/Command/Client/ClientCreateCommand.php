<?php
declare(strict_types=1);

namespace App\Command\Client;

use App\Client\Exception\ClientCreate\ClientValidationException;
use App\Client\Exception\ClientCreate\DuplicateNameException;
use App\Client\Exception\ClientCreateException;
use App\Client\Input\CliClientCreateInput;
use App\Client\Output\CliClientCreateOutput;
use App\Client\UseCase\ClientCreateUseCase;
use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

class ClientCreateCommand extends Command
{

    /**
     * @var ClientCreateUseCase
     */
    private $clientCreate;

    public function __construct(ClientCreateUseCase $clientCreate) {
        parent::__construct();

        $this->clientCreate = $clientCreate;
    }

    protected function configure()
    {
        $this
            ->setName('client:create')
            ->setDescription('Create new client.')
            ->addArgument('name', InputArgument::REQUIRED, 'The clients name.')
        ;
    }

    protected function execute(InputInterface $cliInput, OutputInterface $cliOutput)
    {
        $input = new CliClientCreateInput($cliInput);
        $output = new CliClientCreateOutput($cliOutput);

        try {
            $this->clientCreate->process($input, $output);
        } catch (ClientCreateException $exception) {
            $this->handleClientCreateException($exception, $cliOutput);
        }
    }

    private function handleClientCreateException(ClientCreateException $exception, OutputInterface $cliOutput)
    {
        $messages = [];
        if ($exception instanceof DuplicateNameException) {
            $messages[] = sprintf('Name %s is already in use', $exception->getName());
        } elseif ($exception instanceof ClientValidationException) {
            $violations = $exception->getViolations();
            /** @var ConstraintViolationInterface $violation */
            foreach ($violations as $violation) {
                $messages[] = sprintf(
                    '%s: %s',
                    $violation->getPropertyPath(),
                    $violation->getMessage()
                );
            }
        } else {
            $messages[] = $exception->getMessage();
        }

        array_walk($messages, function (string &$message) {
            $message = sprintf('<error>%s</error>', $message);
        });
        $cliOutput->writeln($messages);
    }
}
