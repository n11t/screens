<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\Screen\YoutubeScreen;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScreenYoutubeCreateCommand extends Command
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct();
        $this->manager = $managerRegistry->getManagerForClass(YoutubeScreen::class);
    }

    protected function configure()
    {
        $this
            ->setName('screen:youtube:create')
            ->setDescription('Create Youtube screen')
            ->addArgument('name', InputArgument::REQUIRED, 'The screen name.')
            ->addArgument('code', InputArgument::REQUIRED, 'The youtube video code.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $code = $input->getArgument('code');

        $screen = new YoutubeScreen($name, $code);

        $this->manager->persist($screen);
        $this->manager->flush();
    }
}
