<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\Screen\MarkdownScreen;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScreenMarkdownCreateCommand extends Command
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct();

        $this->manager = $managerRegistry->getManagerForClass(MarkdownScreen::class);
    }

    protected function configure()
    {
        $this
            ->setName('screen:markdown:create')
            ->setDescription('Create a markdown screen.')
            ->addArgument('name', InputArgument::REQUIRED, 'The screen name.', null)
            ->addArgument('file', InputArgument::REQUIRED, 'The markdown file to read from.', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $markdown = $this->readFile($input);

        $screen = new MarkdownScreen($name, $markdown);

        $this->manager->persist($screen);
        $this->manager->flush();
    }

    private function readFile(InputInterface $input): string
    {
        $content = file_get_contents($input->getArgument('file'));

        return $content;
    }
}
