<?php
declare(strict_types=1);

namespace App\Command\Screen;

use App\Entity\ClientAssignment\ScreenClientAssignment;
use App\Repository\ClientAssignmentRepository;
use App\Repository\ClientRepository;
use App\Repository\ScreenRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScreenAssignCommand extends Command
{

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ScreenRepository
     */
    private $screenRepository;

    /**
     * @var ClientAssignmentRepository
     */
    private $assignmentRepository;

    public function __construct(
        ManagerRegistry $managerRegistry,
        ClientRepository $clientRepository,
        ScreenRepository $screenRepository,
        ClientAssignmentRepository $assignmentRepository
    ) {
        parent::__construct();
        $this->manager = $managerRegistry->getManagerForClass(ScreenClientAssignment::class);
        $this->clientRepository = $clientRepository;
        $this->screenRepository = $screenRepository;
        $this->assignmentRepository = $assignmentRepository;
    }

    protected function configure()
    {
        $this
            ->setName('screen:assign')
            ->setDescription('Assign screen to clients')
            ->addArgument('screen_id', InputArgument::REQUIRED, 'The screen id.')
            ->addArgument('client_ids', InputArgument::IS_ARRAY, 'The client ids to assign to.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $screen = $this->screenRepository->find($input->getArgument('screen_id'));

        $clients = $this->clientRepository->findBy([
            'id' => $input->getArgument('client_ids'),
        ]);

        foreach ($clients as $client) {
            $assignments = $this->assignmentRepository->findBy([
                'client' => $client
            ]);
            foreach ($assignments as $assignment) {
                $this->manager->remove($assignment);
                $this->manager->flush();
            }

            $assignment = new ScreenClientAssignment($client, $screen);
            $this->manager->persist($assignment);
        }
        $this->manager->flush();
    }
}
