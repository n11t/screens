<?php
declare(strict_types=1);

namespace App\Command\Screen;

use App\Screen\Output\CLIScreenListOutput;
use App\Screen\UseCase\ScreenListUseCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScreenListCommand extends Command
{

    /**
     * @var ScreenListUseCase
     */
    private $useCase;

    public function __construct(ScreenListUseCase $useCase)
    {
        parent::__construct();
        $this->useCase = $useCase;
    }

    protected function configure()
    {
        $this
            ->setName('screen:list')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output = new CLIScreenListOutput($output);

        $this->useCase->process($output);

        $output->render();
    }
}
