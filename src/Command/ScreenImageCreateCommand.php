<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\Screen\ImageScreen;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScreenImageCreateCommand extends Command
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct();

        $this->manager = $managerRegistry->getManagerForClass(ImageScreen::class);
    }

    protected function configure()
    {
        $this
            ->setName('screen:image:create')
            ->setDescription('Create a new Image screen')
            ->addArgument('name', InputArgument::REQUIRED, 'The screen name to identify')
            ->addArgument('src', InputArgument::REQUIRED, 'The src to display. Base64 is recommended.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $src = $input->getArgument('src');

        $screen = new ImageScreen($name, $src);

        $this->manager->persist($screen);
        $this->manager->flush();
    }
}
