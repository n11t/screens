<?php
declare(strict_types=1);

namespace App\Command;

use App\Entity\Screen\TableScreen;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ScreenTableCreateCommand extends Command
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct();

        $this->manager = $managerRegistry->getManagerForClass(TableScreen::class);
    }

    protected function configure()
    {
        $this
            ->setName('screen:table:create')
            ->setDescription('Create a table screen')
            ->addArgument('name', InputArgument::REQUIRED, 'The screen name.')
            ->addArgument('headers', InputArgument::REQUIRED, 'The table header as comma separated string')
            ->addArgument('rows', InputArgument::IS_ARRAY, 'Each entry is one row as comma separated string."1,Larry,the Bird,@twitter"')
            ->addOption('headline', null, InputOption::VALUE_REQUIRED, 'The headline for this table.', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $headers = explode(',', $input->getArgument('headers'));
        $rows = $this->getRows($input);
        $headline = $input->getOption('headline');

        $screen = new TableScreen($name, $headers, $rows);
        $screen->setHeadline($headline);

        $this->manager->persist($screen);
        $this->manager->flush();
    }

    private function getRows(InputInterface $input): array
    {
        $rows = $input->getArgument('rows');

        return array_map(function (string $row) {
            return explode(',', $row);
        }, $rows);
    }

}
