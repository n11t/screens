<?php
declare(strict_types=1);

namespace App\Exception\ClientNotFound;

use App\Exception\ClientNotFoundException;

class ClientNotFoundByIdException extends ClientNotFoundException
{

    /**
     * @var int
     */
    private $clientId;

    public function __construct(int $clientId)
    {
        $message = sprintf('Client not found by id %d', $clientId);

        parent::__construct($message, 404);

        $this->clientId = $clientId;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->clientId;
    }
}
