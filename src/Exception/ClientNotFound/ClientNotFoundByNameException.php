<?php
declare(strict_types=1);

namespace App\Exception\ClientNotFound;

use App\Exception\ClientNotFoundException;

class ClientNotFoundByNameException extends ClientNotFoundException
{

    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $message = sprintf('Client not found by name %s', $name);

        parent::__construct($message, 404);

        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
