<?php
declare(strict_types=1);

namespace App\Entity\ClientAssignment;

use App\Entity\Client;
use App\Entity\ClientAssignment;
use App\Entity\Screen;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ScreenClientScreenAssignment
 * @package App\Entity\ClientAssignment
 *
 * @ORM\Entity()
 * @ORM\Table(name="client_assignment_screen")
 */
class ScreenClientAssignment extends ClientAssignment
{

    /**
     * @var int
     * @ORM\Column(name="screen_id", type="integer")
     */
    private $screenId;

    public function __construct(Client $client, Screen $screen)
    {
        parent::__construct($client);
        $this->screenId = $screen->getId();
    }

    /**
     * @return int
     */
    public function getScreenId(): int
    {
        return $this->screenId;
    }
}
