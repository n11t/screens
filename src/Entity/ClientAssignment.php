<?php
declare(strict_types=1);

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use RuntimeException;

/**
 * Class ClientAssignment
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="client_assignment")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "screen" = "App\Entity\ClientAssignment\ScreenClientAssignment"
 * })
 */
abstract class ClientAssignment
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var Client
     * @ORM\OneToOne(targetEntity="App\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var DateTimeInterface
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    public function __construct(Client $client)
    {
        $this->client = $client;
        try {
            $this->createdAt = new DateTime('now');
        } catch (Exception $exception) {
            throw new RuntimeException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception
            );
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }
}
