<?php
declare(strict_types=1);

namespace App\Entity\Screen;

use App\Entity\Screen;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="screen_table")
 */
class TableScreen extends Screen
{

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $headline = null;

    /**
     * @var string[]
     * @ORM\Column(type="json_array")
     */
    private $headers = [];

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $rows = [];

    public function __construct(string $name, array $headers, array $rows)
    {
        parent::__construct($name);

        $this->headers = $headers;
        $this->rows = $rows;
    }

    /**
     * @return string|null
     */
    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    /**
     * @param string|null $headline
     * @return TableScreen
     */
    public function setHeadline(?string $headline): TableScreen
    {
        $this->headline = $headline;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string[] $headers
     * @return TableScreen
     */
    public function setHeaders(array $headers): TableScreen
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return array
     */
    public function getRows(): array
    {
        return $this->rows;
    }

    /**
     * @param array $rows
     * @return TableScreen
     */
    public function setRows(array $rows): TableScreen
    {
        $this->rows = $rows;
        return $this;
    }
}
