<?php
declare(strict_types=1);

namespace App\Entity\Screen;

use App\Entity\Screen;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class YoutubeScreen
 * @package App\Entity\Screen
 * @ORM\Entity
 * @ORM\Table(name="screen_youtube")
 */
class YoutubeScreen extends Screen
{

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $code;

    public function __construct(string $name, string $code)
    {
        parent::__construct($name);

        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return YoutubeScreen
     */
    public function setCode(string $code): YoutubeScreen
    {
        $this->code = $code;
        return $this;
    }
}
