<?php
declare(strict_types=1);

namespace App\Entity\Screen;

use App\Entity\Screen;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="screen_markdown")
 */
class MarkdownScreen extends Screen
{

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;

    public function __construct(string $name, string $markdown)
    {
        parent::__construct($name);

        $this->content = $markdown;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return MarkdownScreen
     */
    public function setContent(string $content): MarkdownScreen
    {
        $this->content = $content;
        return $this;
    }
}
