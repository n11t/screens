<?php
declare(strict_types=1);

namespace App\Entity\Screen;

use App\Entity\Screen;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="screen_image")
 */
class ImageScreen extends Screen
{

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $src;

    public function __construct(string $name, string $src)
    {
        parent::__construct($name);
        $this->src = $src;
    }

    /**
     * @return string
     */
    public function getSrc(): string
    {
        return $this->src;
    }

    /**
     * @param string $src
     * @return ImageScreen
     */
    public function setSrc(string $src): ImageScreen
    {
        $this->src = $src;
        return $this;
    }
}
