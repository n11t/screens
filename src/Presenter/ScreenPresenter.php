<?php
declare(strict_types=1);

namespace App\Presenter;

use App\Entity\Screen;
use App\Markdown\MarkdownParserInterface;

class ScreenPresenter
{

    /**
     * @var MarkdownParserInterface
     */
    private $markdownParser;

    public function __construct(MarkdownParserInterface $markdownParser)
    {
        $this->markdownParser = $markdownParser;
    }

    public function apiJson(?Screen $screen): array
    {
        if ($screen instanceof Screen\ImageScreen) {
            return $this->imageScreenJson($screen);
        } elseif ($screen instanceof Screen\MarkdownScreen) {
            return $this->markdownScreenJson($screen);
        } elseif ($screen instanceof Screen\TableScreen) {
            return $this->tableScreenJson($screen);
        } elseif ($screen instanceof Screen\YoutubeScreen) {
            return $this->youtubeScreenJson($screen);
        }

        return [
            'data' => null,
        ];
    }

    private function imageScreenJson(?Screen\ImageScreen $screen): array
    {
        return [
            'data' => [
                'type' => 'static-image',
                'id' => $screen->getId(),
                'attributes' => [
                    'src' => $screen->getSrc(),
                ],
            ],
        ];
    }

    private function markdownScreenJson(?Screen\MarkdownScreen $screen): array
    {
        return [
            'data' => [
                'type' => 'static-html',
                'id' => $screen->getId(),
                'attributes' => [
                    'html' => $this->markdownParser->parseMarkdown($screen->getContent()),
                ],
            ],
        ];
    }

    private function tableScreenJson(?Screen\TableScreen $screen)
    {
        return [
            'data' => [
                'type' => 'static-table',
                'id' => $screen->getId(),
                'attributes' => [
                    'headline' => $screen->getHeadline(),
                    'headers' => $screen->getHeaders(),
                    'rows' => $screen->getRows(),
                ],
            ],
        ];
    }

    private function youtubeScreenJson(?Screen\YoutubeScreen $screen): array
    {
        $link = sprintf('https://www.youtube.com/embed/%s?autoplay=1', $screen->getCode());

        return [
            'data' => [
                'type' => 'static-youtube',
                'id' => $screen->getId(),
                'attributes' => [
                    'link' => $link,
                ],
            ],
        ];
    }
}
