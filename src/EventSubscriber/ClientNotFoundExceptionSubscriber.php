<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use App\Exception\ClientNotFound\ClientNotFoundByIdException;
use App\Exception\ClientNotFound\ClientNotFoundByNameException;
use App\Exception\ClientNotFoundException;
use App\Exception\ClientNotLoggedInException;
use Psr\Container\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ClientNotFoundExceptionSubscriber implements EventSubscriberInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelEvent',
        ];
    }

    public function onKernelEvent(ExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof ClientNotFoundByNameException) {
            $response = $this->render('error/client/not_found.html.twig', [
                'name' => $exception->getName(),
            ]);

            $response->setStatusCode(404);

            $event->setResponse($response);
        } elseif ($exception instanceof ClientNotFoundByIdException) {
            $response = $this->render('error/client/not_found.html.twig', [
                'id' => $exception->getClientId(),
            ]);

            $response->setStatusCode(404);

            $event->setResponse($response);
        } elseif ($exception instanceof ClientNotLoggedInException) {
            $response = $this->render('error/client/not_found.html.twig');
            $response->setStatusCode(400);

            $event->setResponse($response);
        }
    }

    private function render(string $template, array $parameter = [], Response $response = null): Response
    {
        if (!$this->container->has('twig')) {
            throw new \LogicException('You can not use the "render" method if the Twig Bundle is not available. Try running "composer require symfony/twig-bundle');
        }

        $content = $this->container->get('twig')->render($template, $parameter);

        if (!$response instanceof Response) {
            $response = new Response();
        }

        $response->setContent($content);

        return $response;
    }
}
