<?php
declare(strict_types=1);

namespace App\Screen\View;

use App\Entity\Screen;

interface ScreenViewFactoryInterface
{

    public function build(Screen $screen): ScreenView;
}
