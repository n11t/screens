<?php
declare(strict_types=1);

namespace App\Screen\View;

use App\Entity\Screen;
use App\Screen\View\ScreenView\ImageScreenView;
use App\Screen\View\ScreenView\MarkdownScreenView;
use App\Screen\View\ScreenView\TableScreenView;
use App\Screen\View\ScreenView\YoutubeScreenView;

class ScreenViewFactory implements ScreenViewFactoryInterface
{

    public function build(Screen $screen): ScreenView
    {
        $view = $this->buildView($screen);
        $view
            ->setId($screen->getId())
            ->setName($screen->getName())
            ->setCreatedAt(clone $screen->getCreatedAt())
        ;

        return $view;
    }

    private function buildView(Screen $screen): ScreenView
    {
        if ($screen instanceof Screen\ImageScreen) {
            $view = new ImageScreenView();
            $view
                ->setSrc($screen->getSrc())
            ;

            return $view;
        } elseif ($screen instanceof Screen\MarkdownScreen) {
            $view = new MarkdownScreenView();
            $view
                ->setContent($screen->getContent())
            ;

            return $view;
        } elseif ($screen instanceof Screen\YoutubeScreen) {
            $view = new YoutubeScreenView();
            $view
                ->setCode($screen->getCode())
            ;

            return $view;
        } elseif ($screen instanceof Screen\TableScreen) {
            $view = new TableScreenView();
            $view
                ->setHeaders($screen->getHeaders())
                ->setRows($screen->getRows())
                ->setHeadline($screen->getHeadline())
            ;

            return $view;
        } else {
            throw new \LogicException('Unable to build view for screen class "' . get_class($screen) . '"');
        }
    }
}
