<?php
declare(strict_types=1);

namespace App\Screen\View;

abstract class ScreenView
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTimeInterface
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ScreenView
     */
    public function setId(int $id): ScreenView
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ScreenView
     */
    public function setName(string $name): ScreenView
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     * @return ScreenView
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): ScreenView
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
