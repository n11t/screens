<?php
declare(strict_types=1);

namespace App\Screen\View\ScreenView;

use App\Screen\View\ScreenView;

class ImageScreenView extends ScreenView
{

    /**
     * @var string
     */
    private $src;

    /**
     * @return string
     */
    public function getSrc(): string
    {
        return $this->src;
    }

    /**
     * @param string $src
     * @return ImageScreenView
     */
    public function setSrc(string $src): ImageScreenView
    {
        $this->src = $src;
        return $this;
    }
}
