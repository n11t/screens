<?php
declare(strict_types=1);

namespace App\Screen\View\ScreenView;

use App\Screen\View\ScreenView;

class MarkdownScreenView extends ScreenView
{

    /**
     * @var string
     */
    private $content;

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return MarkdownScreenView
     */
    public function setContent(string $content): MarkdownScreenView
    {
        $this->content = $content;
        return $this;
    }
}
