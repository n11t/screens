<?php
declare(strict_types=1);

namespace App\Screen\View\ScreenView;

use App\Screen\View\ScreenView;

class YoutubeScreenView extends ScreenView
{

    /**
     * @var string
     */
    private $code;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return YoutubeScreenView
     */
    public function setCode(string $code): YoutubeScreenView
    {
        $this->code = $code;
        return $this;
    }
}
