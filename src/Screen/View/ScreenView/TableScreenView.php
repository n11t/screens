<?php
declare(strict_types=1);

namespace App\Screen\View\ScreenView;

use App\Screen\View\ScreenView;

class TableScreenView extends ScreenView
{

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var array
     */
    private $rows = [];

    /**
     * @var string|null
     */
    private $headline = null;

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     * @return TableScreenView
     */
    public function setHeaders(array $headers): TableScreenView
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return array
     */
    public function getRows(): array
    {
        return $this->rows;
    }

    /**
     * @param array $rows
     * @return TableScreenView
     */
    public function setRows(array $rows): TableScreenView
    {
        $this->rows = $rows;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    /**
     * @param string|null $headline
     * @return TableScreenView
     */
    public function setHeadline(?string $headline): TableScreenView
    {
        $this->headline = $headline;
        return $this;
    }
}
