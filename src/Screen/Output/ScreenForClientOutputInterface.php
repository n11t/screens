<?php
declare(strict_types=1);

namespace App\Screen\Output;

use App\Entity\Screen;

interface ScreenForClientOutputInterface
{

    public function setScreen(Screen $screen): void;
}
