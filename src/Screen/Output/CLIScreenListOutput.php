<?php
declare(strict_types=1);

namespace App\Screen\Output;

use App\Screen\View\ScreenView;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class CLIScreenListOutput implements ScreenListOutputInterface
{

    /**
     * @var Table
     */
    private $table;

    /**
     * @var ScreenView[]
     */
    private $screens = [];

    public function __construct(OutputInterface $cliOutput)
    {
        $this->table = new Table($cliOutput);
        $this->table->setHeaders([
            '#',
            'Nane',
            'Type',
            'Created'
        ]);
    }

    public function addScreen(ScreenView $view): void
    {
        $this->table->addRow($this->buildRow($view));
    }

    public function render(): void
    {
        $this->table->render();
    }

    private function buildRow(ScreenView $view)
    {
        return [
            $view->getId(),
            $view->getName(),
            $this->getScreenType($view),
            $view->getCreatedAt()->format('Y-m-d H:i:s')
        ];
    }

    private function getScreenType(ScreenView $view): string
    {
        if ($view instanceof ScreenView\TableScreenView) {
            return 'table';
        } elseif ($view instanceof ScreenView\YoutubeScreenView) {
            return 'youtube';
        } elseif ($view instanceof ScreenView\MarkdownScreenView) {
            return 'markdown';
        } elseif ($view instanceof ScreenView\ImageScreenView) {
            return 'image';
        }

        return 'unknown';
    }
}
