<?php
declare(strict_types=1);

namespace App\Screen\Output;

use App\Entity\Screen;

class ScreenForClientOutput implements ScreenForClientOutputInterface
{

    /**
     * @var Screen|null
     */
    public $screen;

    public function setScreen(Screen $screen): void
    {
        $this->screen = $screen;
    }
}
