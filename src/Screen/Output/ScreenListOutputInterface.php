<?php
declare(strict_types=1);

namespace App\Screen\Output;

use App\Screen\View\ScreenView;

interface ScreenListOutputInterface
{

    public function addScreen(ScreenView $screenView): void;
}
