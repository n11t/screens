<?php
declare(strict_types=1);

namespace App\Screen\Gateway;

use App\Entity\Screen;

interface ScreenListGatewayInterface
{

    /**
     * @return Screen[]
     */
    public function findScreens(): array;
}
