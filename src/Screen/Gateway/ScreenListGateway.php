<?php
declare(strict_types=1);

namespace App\Screen\Gateway;

use App\Entity\Screen;
use App\Repository\ScreenRepository;

class ScreenListGateway implements ScreenListGatewayInterface
{

    /**
     * @var ScreenRepository
     */
    private $screenRepository;

    public function __construct(ScreenRepository $screenRepository)
    {
        $this->screenRepository = $screenRepository;
    }

    /**
     * @inheritDoc
     */
    public function findScreens(): array
    {
        return $this->screenRepository->findAll();
    }
}
