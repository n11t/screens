<?php
declare(strict_types=1);

namespace App\Screen\Gateway;

use App\Entity\Client;
use App\Entity\ClientAssignment;
use App\Entity\Screen;

interface ScreenForClientGatewayInterface
{

    public function findAssignment(Client $client): ?ClientAssignment;

    public function findClient(int $clientId): ?Client;

    public function findScreen(int $screenId): ?Screen;
}
