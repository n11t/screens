<?php
declare(strict_types=1);

namespace App\Screen\Gateway;

use App\Entity\Client;
use App\Entity\ClientAssignment;
use App\Entity\Screen;
use App\Repository\ClientAssignmentRepository;
use App\Repository\ClientRepository;
use App\Repository\ScreenRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ScreenForClientGateway implements ScreenForClientGatewayInterface
{

    /**
     * @var ClientAssignmentRepository
     */
    private $assignmentRepository;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ScreenRepository
     */
    private $screenRepository;

    public function __construct(
        ClientAssignmentRepository $assignmentRepository,
        ClientRepository $clientRepository,
        ScreenRepository $screenRepository
    ) {
        $this->assignmentRepository = $assignmentRepository;
        $this->clientRepository = $clientRepository;
        $this->screenRepository = $screenRepository;
    }

    public function findAssignment(Client $client): ?ClientAssignment
    {
        return $this->assignmentRepository->findOneBy([
            'client' => $client,
        ]);
    }

    public function findClient(int $id): ?Client
    {
        return $this->clientRepository->find($id);
    }

    public function findScreen(int $screenId): ?Screen
    {
        return $this->screenRepository->find($screenId);
    }
}
