<?php
declare(strict_types=1);

namespace App\Screen\Input;

interface ScreenForClientInputInterface
{

    public function getClientId(): int;
}
