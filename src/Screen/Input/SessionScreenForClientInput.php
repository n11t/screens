<?php
declare(strict_types=1);

namespace App\Screen\Input;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionScreenForClientInput implements ScreenForClientInputInterface
{

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function getClientId(): int
    {
        return (int)$this->session->get('client.id');
    }
}
