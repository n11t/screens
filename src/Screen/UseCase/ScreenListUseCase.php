<?php
declare(strict_types=1);

namespace App\Screen\UseCase;

use App\Screen\Gateway\ScreenListGatewayInterface;
use App\Screen\Output\ScreenListOutputInterface;
use App\Screen\View\ScreenViewFactoryInterface;

class ScreenListUseCase
{

    /**
     * @var ScreenListGatewayInterface
     */
    private $gateway;

    /**
     * @var ScreenViewFactoryInterface
     */
    private $screenViewFactory;

    public function __construct(
        ScreenListGatewayInterface $gateway,
        ScreenViewFactoryInterface $screenViewFactory)
    {
        $this->gateway = $gateway;
        $this->screenViewFactory = $screenViewFactory;
    }

    public function process(ScreenListOutputInterface $output): void
    {
        $screens = $this->findScreens();

        foreach ($screens as $screen) {
            $screenView = $this->screenViewFactory->build($screen);

            $output->addScreen($screenView);
        }
    }

    private function findScreens(): array
    {
        return $this->gateway->findScreens();
    }
}
