<?php
declare(strict_types=1);

namespace App\Screen\UseCase;

use App\Entity\Client;
use App\Entity\ClientAssignment;
use App\Entity\ClientAssignment\ScreenClientAssignment;
use App\Entity\Screen;
use App\Screen\Gateway\ScreenForClientGatewayInterface;
use App\Screen\Input\ScreenForClientInputInterface;
use App\Screen\Output\ScreenForClientOutputInterface;

class ScreenForClientUseCase
{

    /**
     * @var ScreenForClientGatewayInterface
     */
    private $gateway;

    public function __construct(ScreenForClientGatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }

    public function process(ScreenForClientInputInterface $input, ScreenForClientOutputInterface $output): void
    {
        $assignment = $this->findAssignment($input);

        $screen = null;
        if ($assignment instanceof ScreenClientAssignment) {
            $screen = $this->findScreen($assignment->getScreenId());
        }

        if ($screen instanceof Screen) {
            $output->setScreen($screen);
        }
    }

    private function findAssignment(ScreenForClientInputInterface $input): ?ClientAssignment
    {
        $client = $this->findClient($input->getClientId());
        if (!$client instanceof Client) {
            return null;
        }

        return $this->gateway->findAssignment($client);
    }

    private function findClient($clientId): ?Client
    {
        return $this->gateway->findClient($clientId);
    }

    private function findScreen(int $screenId): ?Screen
    {
        return $this->gateway->findScreen($screenId);
    }
}
