<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\ClientAssignment\ScreenClientAssignment;
use App\Entity\Screen\MarkdownScreen;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $content = <<<MARKDOWN
# Welcome to screen

You can create new screens via `bin/console screen:{TYPE}:create` while TYPE can be one of
- image
- markdown
- table
- youtube

Screens can be assigned to a client via `bin/console screen:assign SCREEN_ID CLIENT_ID [CLIENT_ID [CLIENT_ID]]`  

Let's create a new image screen replace `IMAGE_URL`:
```
bin/console screen:image:create IMAGE_URL
```
MARKDOWN;
        $screen = new MarkdownScreen($content);
        $manager->persist($screen);
        $manager->flush();

        $client = new Client();
        $client->setName('demo');
        $manager->persist($client);
        $manager->flush();

        $assignment = new ScreenClientAssignment($client, $screen);
        $manager->persist($assignment);
        $manager->flush();
    }
}
