<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\ClientAssignment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class ClientAssignmentRepository
 * @package App\Repository
 *
 * @method ClientAssignment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientAssignment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientAssignment[] findAll()
 * @method ClientAssignment[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientAssignmentRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientAssignment::class);
    }
}
