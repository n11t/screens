<?php
declare(strict_types=1);

namespace App\Client\Output;

use App\Entity\Client;

interface ClientCreateOutputInterface
{

    /**
     * @param Client $client
     */
    public function setClient(Client $client): void;
}
