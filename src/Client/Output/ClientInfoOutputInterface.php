<?php
declare(strict_types=1);

namespace App\Client\Output;

use App\Client\View\DetailClientView;

interface ClientInfoOutputInterface
{

    public function setClient(DetailClientView $view): void;
}
