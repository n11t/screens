<?php
declare(strict_types=1);

namespace App\Client\Output;

use App\Entity\Client;
use Symfony\Component\Console\Output\OutputInterface;

class CliClientCreateOutput implements ClientCreateOutputInterface
{

    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * @inheritDoc
     */
    public function setClient(Client $client): void
    {
        $message = sprintf('<info>Client with id %d created.</info>', $client->getId());

        $this->output->writeln($message);
    }
}
