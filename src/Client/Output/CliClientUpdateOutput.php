<?php
declare(strict_types=1);

namespace App\Client\Output;

use App\Entity\Client;
use Symfony\Component\Console\Output\OutputInterface;

class CliClientUpdateOutput implements ClientUpdateOutputInterface
{

    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function setClient(Client $client): void
    {
        $message = sprintf(
            '<info>Client with id %d updated.</info>',
            $client->getId()
        );

        $this->output->writeln($message);
    }
}
