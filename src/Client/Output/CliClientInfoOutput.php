<?php
declare(strict_types=1);

namespace App\Client\Output;

use App\Client\View\DetailClientView;
use App\Entity\Screen;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class CliClientInfoOutput implements ClientInfoOutputInterface
{

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var DetailClientView
     */
    private $view;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function setClient(DetailClientView $view): void
    {
        $this->view = $view;
    }

    public function render(): void
    {
        $this->writeProperties($this->output, [
            'name' => $this->view->getName(),
            'created at' => $this->view->getCreatedAt()->format('Y-m-d H:i:s'),
        ]);

        $this->output->writeln('');

        $this->writeScreen($this->output, $this->view->getScreen());
    }

    private function writeProperties(OutputInterface $output, array $properties = []): void
    {
        $maxLength = 0;
        foreach ($properties as $key => $value) {
            $len = strlen($key);
            if ($len > $maxLength) {
                $maxLength = $len;
            }
        }

        foreach ($properties as $key => $value) {
            $lenSpaces = ($maxLength - strlen($key)) + 1;
            $message = sprintf(
                '<info>%s</info>%s: %s',
                $key,
                str_repeat(' ', $lenSpaces),
                $value
            );

            $output->writeln($message);
        }
    }

    private function writeScreen(OutputInterface $output, ?Screen $screen): void
    {
        if (!$screen instanceof Screen) {
            return;
        }
        $output->writeln('<info>Screen</info>');

        $output->writeln(sprintf('name <comment>%s</comment>', $screen->getName()));

        if ($screen instanceof Screen\TableScreen) {
            $output->writeln('type <comment>table</comment>');
            $output->writeln('');

            $table = new Table($output);
            $table->setHeaderTitle($screen->getHeadline());
            $table->setHeaders($screen->getHeaders());
            $table->setRows($screen->getRows());

            $table->render();
        } elseif ($screen instanceof Screen\YoutubeScreen) {
            $output->writeln('type <comment>youtube</comment>');
            $output->writeln(sprintf('code <comment>%s</comment>', $screen->getCode()));
        } elseif ($screen instanceof Screen\ImageScreen) {
            $output->writeln('type <comment>image</comment>');
            $source = $screen->getSrc();
            if (strlen($source) > 75) {
                $source = substr($source, 0, 72) . '...';
            }
            $output->writeln(sprintf('source <comment>%s</comment>', $source));
        } elseif ($screen instanceof Screen\MarkdownScreen) {
            $output->writeln('type <comment>markdown</comment>');
        }
    }
}
