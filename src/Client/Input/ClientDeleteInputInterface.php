<?php
declare(strict_types=1);

namespace App\Client\Input;

interface ClientDeleteInputInterface
{

    public function getId(): int;
}
