<?php
declare(strict_types=1);

namespace App\Client\Input;

use Symfony\Component\Console\Input\InputInterface;

class CliClientUpdateInput implements ClientUpdateInputInterface
{

    /**
     * @var InputInterface
     */
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    public function getId(): int
    {
        return (int)$this->input->getArgument('id');
    }

    public function getName(): ?string
    {
        return $this->input->getOption('name');
    }
}
