<?php
declare(strict_types=1);

namespace App\Client\Input;

interface ClientCreateInputInterface
{

    public function getName(): string;
}
