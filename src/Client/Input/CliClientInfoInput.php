<?php
declare(strict_types=1);

namespace App\Client\Input;

use Symfony\Component\Console\Input\InputInterface;

class CliClientInfoInput implements ClientInfoInputInterface
{

    /**
     * @var InputInterface
     */
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    public function getClientId(): int
    {
        return (int)$this->input->getArgument('client_id');
    }
}
