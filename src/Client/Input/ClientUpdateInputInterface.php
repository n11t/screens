<?php
declare(strict_types=1);

namespace App\Client\Input;

interface ClientUpdateInputInterface
{

    public function getId(): int;

    public function getName(): ?string;
}
