<?php
declare(strict_types=1);

namespace App\Client\Input;

use Symfony\Component\Console\Input\InputInterface;

class CliClientCreateInput implements ClientCreateInputInterface
{

    /**
     * @var InputInterface
     */
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    public function getName(): string
    {
        return $this->input->getArgument('name');
    }
}
