<?php
declare(strict_types=1);

namespace App\Client\Input;

interface ClientInfoInputInterface
{

    public function getClientId(): int;
}
