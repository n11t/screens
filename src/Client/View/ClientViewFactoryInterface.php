<?php
declare(strict_types=1);

namespace App\Client\View;

use App\Entity\Client;

interface ClientViewFactoryInterface
{

    public function build(Client $client): ClientView;

    public function buildDetail(Client $client): DetailClientView;
}
