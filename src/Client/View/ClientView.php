<?php
declare(strict_types=1);

namespace App\Client\View;

class ClientView
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTimeInterface
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ClientView
     */
    public function setId(int $id): ClientView
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ClientView
     */
    public function setName(string $name): ClientView
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     * @return ClientView
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): ClientView
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
