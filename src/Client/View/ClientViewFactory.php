<?php
declare(strict_types=1);

namespace App\Client\View;

use App\Entity\Client;
use App\Entity\Screen;
use App\Screen\Input\ScreenForClientInputInterface;
use App\Screen\Output\ScreenForClientOutputInterface;
use App\Screen\UseCase\ScreenForClientUseCase;

class ClientViewFactory implements ClientViewFactoryInterface
{

    /**
     * @var ScreenForClientUseCase
     */
    private $screenForClientUseCase;

    public function __construct(ScreenForClientUseCase $screenForClientUseCase)
    {
        $this->screenForClientUseCase = $screenForClientUseCase;
    }

    public function build(Client $client): ClientView
    {
        $view = new ClientView();

        $this->fillView($view, $client);

        return $view;
    }

    public function buildDetail(Client $client): DetailClientView
    {
        $view = new DetailClientView();
        $this->fillView($view, $client);

        $screen = $this->screenForClient($client);
        $view->setScreen($screen);

        return $view;
    }

    private function fillView(ClientView $view, Client $client): void
    {
        $view
            ->setId($client->getId())
            ->setName($client->getName())
            ->setCreatedAt(clone $client->getCreatedAt())
        ;
    }

    private function screenForClient(Client $client): ?Screen
    {
        $input = new class($client->getId()) implements ScreenForClientInputInterface {
            private $id;

            public function __construct(int $id)
            {
                $this->id = $id;
            }

            public function getClientId(): int
            {
                return $this->id;
            }
        };
        $output = new class implements ScreenForClientOutputInterface {
            public $screen = null;

            public function setScreen(Screen $screen): void
            {
                $this->screen = $screen;
            }
        };

        $this->screenForClientUseCase->process($input, $output);

        return $output->screen;
    }
}
