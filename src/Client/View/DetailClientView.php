<?php
declare(strict_types=1);

namespace App\Client\View;

use App\Entity\Screen;

class DetailClientView extends ClientView
{

    /**
     * @var ?Screen
     */
    private $screen;

    /**
     * @return mixed
     */
    public function getScreen()
    {
        return $this->screen;
    }

    /**
     * @param mixed $screen
     * @return DetailClientView
     */
    public function setScreen($screen)
    {
        $this->screen = $screen;
        return $this;
    }
}
