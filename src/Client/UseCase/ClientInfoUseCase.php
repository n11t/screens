<?php
declare(strict_types=1);

namespace App\Client\UseCase;

use App\Client\Exception\ClientInfo\ClientNotFoundException;
use App\Client\Exception\ClientInfoException;
use App\Client\Gateway\ClientInfoGatewayInterface;
use App\Client\Input\ClientInfoInputInterface;
use App\Client\Output\ClientInfoOutputInterface;
use App\Client\View\ClientViewFactoryInterface;
use App\Entity\Client;

class ClientInfoUseCase
{

    /**
     * @var ClientInfoGatewayInterface
     */
    private $gateway;
    /**
     * @var ClientViewFactoryInterface
     */
    private $clientViewFactory;

    public function __construct(ClientInfoGatewayInterface $gateway, ClientViewFactoryInterface $clientViewFactory)
    {
        $this->gateway = $gateway;
        $this->clientViewFactory = $clientViewFactory;
    }

    /**
     * @param ClientInfoInputInterface $input
     * @param ClientInfoOutputInterface $output
     * @throws ClientInfoException
     */
    public function process(ClientInfoInputInterface $input, ClientInfoOutputInterface $output): void
    {
        $client = $this->findClient($input);

        $view = $this->clientViewFactory->buildDetail($client);

        $output->setClient($view);
    }

    /**
     * @param ClientInfoInputInterface $input
     * @return Client
     * @throws ClientNotFoundException
     */
    private function findClient(ClientInfoInputInterface $input): Client
    {
        $client = $this->gateway->findClient($input->getClientId());

        if (!$client instanceof Client) {
            throw new ClientNotFoundException($input->getClientId());
        }

        return $client;
    }
}
