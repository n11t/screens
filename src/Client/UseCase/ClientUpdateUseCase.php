<?php
declare(strict_types=1);

namespace App\Client\UseCase;

use App\Client\Exception\ClientUpdate\ClientNotFoundException;
use App\Client\Exception\ClientUpdate\ClientValidationException;
use App\Client\Exception\ClientUpdate\DuplicateNameException;
use App\Client\Exception\ClientUpdateException;
use App\Client\Gateway\ClientUpdateGatewayInterface;
use App\Client\Input\ClientUpdateInputInterface;
use App\Client\Output\ClientUpdateOutputInterface;
use App\Entity\Client;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClientUpdateUseCase
{

    /**
     * @var ClientUpdateGatewayInterface
     */
    private $gateway;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ClientUpdateGatewayInterface $gateway, ValidatorInterface $validator)
    {
        $this->gateway = $gateway;
        $this->validator = $validator;
    }

    /**
     * @param ClientUpdateInputInterface $input
     * @param ClientUpdateOutputInterface $output
     * @throws ClientUpdateException
     */
    public function process(ClientUpdateInputInterface $input, ClientUpdateOutputInterface $output): void
    {
        $client = $this->findClient($input);

        $this->updateClient($client, $input);

        $this->validateClient($client);

        $this->persistClient($client);

        $output->setClient($client);
    }

    /**
     * @param ClientUpdateInputInterface $input
     * @return Client
     * @throws ClientNotFoundException
     */
    private function findClient(ClientUpdateInputInterface $input): Client
    {
        $client = $this->gateway->findClient($input->getId());
        if (!$client instanceof Client) {
            throw new ClientNotFoundException($input->getId());
        }

        return $client;
    }

    private function updateClient(Client $client, ClientUpdateInputInterface $input): void
    {
        $name = $input->getName();
        if (is_string($name)) {
            $client->setName($name);
        }
    }

    /**
     * @param Client $client
     * @throws DuplicateNameException
     * @throws ClientValidationException
     */
    private function validateClient(Client $client): void
    {
        $clients = $this->gateway->findClientsByName($client->getName());
        if (count($clients) > 0) {
            throw new DuplicateNameException($client->getName());
        }

        $violations = $this->validator->validate($client);
        if (count($violations) > 0) {
            throw new ClientValidationException($violations);
        }
    }

    private function persistClient(Client $client): void
    {
        $this->gateway->persistClient($client);
    }
}
