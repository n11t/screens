<?php
declare(strict_types=1);

namespace App\Client\UseCase;

use App\Client\Exception\ClientCreate\ClientValidationException;
use App\Client\Exception\ClientCreate\DuplicateNameException;
use App\Client\Exception\ClientCreateException;
use App\Client\Gateway\ClientCreateGatewayInterface;
use App\Client\Input\ClientCreateInputInterface;
use App\Client\Output\ClientCreateOutputInterface;
use App\Entity\Client;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClientCreateUseCase
{

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ClientCreateGatewayInterface
     */
    private $gateway;

    public function __construct(ClientCreateGatewayInterface $gateway, ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->gateway = $gateway;
    }

    /**
     * @param ClientCreateInputInterface $input
     * @param ClientCreateOutputInterface $output
     * @throws ClientCreateException
     */
    public function process(ClientCreateInputInterface $input, ClientCreateOutputInterface $output): void
    {
        $client = $this->buildClient($input);

        $this->validateClient($client);

        $this->persistClient($client);

        $output->setClient($client);
    }

    private function buildClient(ClientCreateInputInterface $input): Client
    {
        $client = new Client();
        $client->setName($input->getName());

        return $client;
    }

    /**
     * @param Client $client
     * @throws ClientValidationException
     * @throws DuplicateNameException
     */
    private function validateClient(Client $client): void
    {
        $name = $client->getName();
        $clients = $this->gateway->findClientsByName($name);
        if (count($clients) > 0) {
            throw new DuplicateNameException($client->getName());
        }

        $violations = $this->validator->validate($client);
        if (count($violations) > 0) {
            throw new ClientValidationException($violations);
        }
    }

    private function persistClient(Client $client): void
    {
        $this->gateway->persistClient($client);
    }
}
