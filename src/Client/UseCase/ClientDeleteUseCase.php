<?php
declare(strict_types=1);

namespace App\Client\UseCase;

use App\Client\Gateway\ClientDeleteGatewayInterface;
use App\Client\Input\ClientDeleteInputInterface;
use App\Entity\Client;

class ClientDeleteUseCase
{

    /**
     * @var ClientDeleteGatewayInterface
     */
    private $gateway;

    public function __construct(ClientDeleteGatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }

    public function process(ClientDeleteInputInterface $input): void
    {
        $client = $this->findClient($input);
        if (!$client instanceof Client) {
            return;
        }

        $this->deleteClient($client);
    }

    private function findClient(ClientDeleteInputInterface $input): ?Client
    {
        return $this->gateway->findClient($input->getId());
    }

    private function deleteClient(Client $client): void
    {
        $this->gateway->deleteClient($client);
    }
}
