<?php
declare(strict_types=1);

namespace App\Client\Exception\ClientUpdate;

use App\Client\Exception\ClientUpdateException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class ClientValidationException extends ClientUpdateException
{

    /**
     * @var ConstraintViolationListInterface
     */
    private $violations;

    public function __construct(ConstraintViolationListInterface $violations)
    {
        $message = sprintf('Unable to update client.');

        parent::__construct($message, 400);

        $this->violations = $violations;
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
