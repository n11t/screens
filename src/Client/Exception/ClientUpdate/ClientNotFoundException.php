<?php
declare(strict_types=1);

namespace App\Client\Exception\ClientUpdate;

use App\Client\Exception\ClientUpdateException;

class ClientNotFoundException extends ClientUpdateException
{

    /**
     * @var int
     */
    private $id;

    public function __construct(int $id)
    {
        $message = sprintf('No client found for id %d', $id);

        parent::__construct($message, 404);

        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
