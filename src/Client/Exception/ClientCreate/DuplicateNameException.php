<?php
declare(strict_types=1);

namespace App\Client\Exception\ClientCreate;

use App\Client\Exception\ClientCreateException;
use Throwable;

class DuplicateNameException extends ClientCreateException
{

    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $message = sprintf('Name %s is already in use.', $name);

        parent::__construct($message, 400);

        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
