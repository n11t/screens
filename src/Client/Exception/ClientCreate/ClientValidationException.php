<?php
declare(strict_types=1);

namespace App\Client\Exception\ClientCreate;

use App\Client\Exception\ClientCreateException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class ClientValidationException extends ClientCreateException
{

    /**
     * @var ConstraintViolationListInterface
     */
    private $violations;

    public function __construct(ConstraintViolationListInterface $violations)
    {
        $message = sprintf('Unable to create client.');

        parent::__construct($message, 404);

        $this->violations = $violations;
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
