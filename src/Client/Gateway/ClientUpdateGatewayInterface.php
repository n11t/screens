<?php
declare(strict_types=1);

namespace App\Client\Gateway;

use App\Entity\Client;

interface ClientUpdateGatewayInterface
{

    public function findClient(int $id): ?Client;

    /**
     * @param string $name
     * @return Client[]
     */
    public function findClientsByName(string $name): array;

    public function persistClient(Client $client): void;
}
