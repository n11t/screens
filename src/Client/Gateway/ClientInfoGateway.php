<?php
declare(strict_types=1);

namespace App\Client\Gateway;

use App\Entity\Client;
use App\Repository\ClientRepository;

class ClientInfoGateway implements ClientInfoGatewayInterface
{

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function findClient(int $id): ?Client
    {
        return $this->clientRepository->find($id);
    }
}
