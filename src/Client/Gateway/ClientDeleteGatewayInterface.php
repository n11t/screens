<?php
declare(strict_types=1);

namespace App\Client\Gateway;

use App\Entity\Client;

interface ClientDeleteGatewayInterface
{

    public function findClient($id): ?Client;

    public function deleteClient(Client $client): void;
}
