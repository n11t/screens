<?php
declare(strict_types=1);

namespace App\Client\Gateway;

use App\Entity\Client;

interface ClientInfoGatewayInterface
{

    public function findClient(int $id): ?Client;
}
