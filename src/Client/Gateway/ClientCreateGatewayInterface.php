<?php
declare(strict_types=1);

namespace App\Client\Gateway;

use App\Entity\Client;

interface ClientCreateGatewayInterface
{

    /**
     * @param Client $client
     */
    public function persistClient(Client $client): void;

    /**
     * @param string $name
     * @return Client[]
     */
    public function findClientsByName(string $name): array;
}
