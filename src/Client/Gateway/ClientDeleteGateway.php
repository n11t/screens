<?php
declare(strict_types=1);

namespace App\Client\Gateway;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

class ClientDeleteGateway implements ClientDeleteGatewayInterface
{

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $managerRegistry, ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->manager = $managerRegistry->getManagerForClass(Client::class);
    }

    public function findClient($id): ?Client
    {
        return $this->clientRepository->find($id);
    }

    public function deleteClient(Client $client): void
    {
        $this->manager->remove($client);
        $this->manager->flush();
    }
}
