<?php
declare(strict_types=1);

namespace App\Client\Gateway;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

class ClientUpdateGateway implements ClientUpdateGatewayInterface
{

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    public function __construct(ManagerRegistry $managerRegistry, ClientRepository $clientRepository)
    {
        $this->manager = $managerRegistry->getManagerForClass(Client::class);
        $this->clientRepository = $clientRepository;
    }

    public function findClient(int $id): ?Client
    {
        return $this->clientRepository->find($id);
    }

    /**
     * @param string $name
     * @return Client[]
     */
    public function findClientsByName(string $name): array
    {
        return $this->clientRepository->findBy([
            'name' => $name,
        ]);
    }

    public function persistClient(Client $client): void
    {
        $this->manager->persist($client);
        $this->manager->flush();
    }
}
