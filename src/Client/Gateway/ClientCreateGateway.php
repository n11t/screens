<?php
declare(strict_types=1);

namespace App\Client\Gateway;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

class ClientCreateGateway implements ClientCreateGatewayInterface
{

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    public function __construct(ManagerRegistry $managerRegistry, ClientRepository $clientRepository)
    {
        $this->manager = $managerRegistry->getManagerForClass(Client::class);
        $this->clientRepository = $clientRepository;
    }

    public function persistClient(Client $client): void
    {
        $this->manager->persist($client);
        $this->manager->flush();
    }

    public function findClientsByName(string $name): array
    {
        return $this->clientRepository->findBy([
            'name' => $name,
        ]);
    }
}
