<?php
declare(strict_types=1);

namespace App\Markdown;

interface MarkdownParserInterface
{

    /**
     * Parse given markdown to html.
     *
     * @param string $markdown
     * @return string
     */
    public function parseMarkdown(string $markdown): string;
}
