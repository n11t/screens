<?php
declare(strict_types=1);

namespace App\Markdown;

class ParsedownMarkdownParser implements MarkdownParserInterface
{

    /**
     * @inheritDoc
     */
    public function parseMarkdown(string $markdown): string
    {
        $parsedown = new \Parsedown();

        return $parsedown->parse($markdown);
    }
}
